# GB3D

An experimental CAD/3D viewer for use in [GitBuilding](https://gitbuilding.io). It is built using Three.js. You can [see it in action here](https://gitbuilding.gitlab.io/gb3d/).

![](screenshot.png)

## Features

* Renders STL files, glTF files (.glb) and VRML files (.wrl). Unfortunately cannot render STEP files. We use [FreeCAD](https://freecadweb.org/) to convert these.
* Unrestrained rotation.
* Render on demand - Only renders when the model is moved for low CPU usage.
* Slider for exploded 3D diagrams. (See below)


## Making 3D assemblies that can be exploded

The 3D explosion viewer works only for glTF files, we use [Blender](https://www.blender.org/) to generate glTF files.

Currently generating the exploded assembly requires manually making and animation of the explosion in a program such as blender.

To make an exploding assembly:

* Assemble the components in blender (or export the assembled parts from CAD and import into Blender)
* [Create an animation](https://docs.blender.org/manual/en/latest/animation/index.html) of the components moving from the assembled position to the desired exploded position. Do not make the parts rotate or add extra keyframes. GB3D only interpolates from the initial to the final position ignoring changes to rotation or scale.
* Export the scene as a glTF file (.glb).
* Load the file into GB3D using the HTML below, the explosion slider should appear automatically.

We understand that the process to make the exploded assemblies is not simple. If you have any ideas of tools or methods to simplify this please open an issue.

## Using GB3D


### Install 

You can either install from NPM or include the bundle. 

#### From NPM

Run

```
npm install gb3d
```

#### Include bundle

[Download the bundled javascript from the example website](https://gibuilding.gitlab.io/gb3d/gb3d_bundle.js) or build the bundle locally (see below).

Once you have the bundle you can add a GB3D to an HTML page by copying the `gb3d_bundle.js` file and the model you want to display into the folder of the webpage and adding this to the `HEAD` of the HTML page:

    <script type="text/javascript" src="./gb3d_bundle.js"></script>

### Usage

Add this where you want the viewer:

    <gb3d-viewer src="MODELNAME.stl" ></gb3d-viewer>

remembering to update the model name.

For STLs, you can also set the model colour using hex codes or web colours, for example:

    <gb3d-viewer src="MODELNAME.stl" color="deeppink"></gb3d-viewer>

Your website will need a server to serve the model due to security restrictions, you can't just open the HTML file from disk. For more information see [the three.js documentation on running locally](https://threejs.org/docs/#manual/en/introduction/How-to-run-things-locally).


## To build locally

To build locally you need Node/npm. This has been tested with Node v12.4.0 and npm v6.14.4. If you have problems we recommend trying the current LTS version of Node which can be downloaded from [nodejs.org](https://nodejs.org/en/download/).

To build, clone this repository, navigate to into the gb3d directory and run:

    npm install
    npm run build

You can then test the result by configuring [git-lfs](https://git-lfs.github.com/), checking out the test files and running:

    npm run serve

Then navigate to http://localhost:8000

## Credits

GB3D was written by the Bath Open INstrumentation Group.

The main viewer was initially based on the [three.js VRML loader example](https://threejs.org/examples/#webgl_loader_vrml) and has taken a lot of inspiration (and some code) from Justin Ribeiro's [stl-part-viewer](https://github.com/justinribeiro/stl-part-viewer) and Don McCurdy's [three-gltf-viewer](https://github.com/donmccurdy/three-gltf-viewer/). The navigation tools are modified from the [three.js trackball controls](https://threejs.org/examples/#misc_controls_trackball).

GB3D is released under the MIT license.
