import resolve from '@rollup/plugin-node-resolve'
import image from '@rollup/plugin-image';

export default {
  input: 'gb3d.js',
  output: {
    format: 'umd',
    file: './gb3d_bundle.js'
  },
  plugins: [resolve(), image()]
}
