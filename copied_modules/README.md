These modules are copied from the Three.js examples. Currently they cannot be used by rollup directly from the npm install because of ES6 compatibility issues.

See [here](https://github.com/mrdoob/three.js/issues/9562#issuecomment-386522819) and for chevrotain see [here](https://github.com/SAP/chevrotain/issues/976).
