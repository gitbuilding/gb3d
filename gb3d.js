
import { LitElement, html } from '@polymer/lit-element'
import * as THREE from 'three'
import { TrackballControls } from './GBTrackballControls.js'
import { VRMLLoader } from './copied_modules/VRMLLoader.js'
import { STLLoader } from './copied_modules/STLLoader.js'
import { GLTFLoader } from './copied_modules/GLTFLoader.js'
import machineshop from './machine_shop_01_1k.jpg'

class GB3DViewer extends LitElement {
  static get properties () {
    return {
      src: String,
      color: String,
      'show-filelink': Boolean,
      width: String,
      height: String
    }
  }

  render () {
    return html`<style>
        :host {
          display: block;
          position: relative;
          width: 600px;
          height: 400px;
          line-height: 0;
        }
        #info-box,
        #viewport {
          width: 100%;
          height: 100%;
          position: absolute;
          top: 0;
          left: 0;
        }
        #info-box {
          padding-top: 9px;
          line-height: 18px;
          text-align: center;
          font-size: 12pt;
        }
        #filelink {
            position: relative;
            z-index: 10;
            text-decoration: underline;
            color: black;
            background-color: #fff9
        }
        #explodeslider {
            -webkit-appearance: none;
            width: 80%;
            height: 25px;
            background: #d3d3d3;
            outline: none;
            opacity: 0.7;
            z-index: 10;
            position: absolute;
            bottom: 5%;
            left: 10%;
            -webkit-transition: .2s;
            transition: opacity .2s;
            visibility: hidden;
        }
        #explodeslider:hover {
            opacity: 1;
        }
        #explodeslider::-webkit-slider-thumb {
            -webkit-appearance: none;
            appearance: none;
            width: 25px;
            height: 25px;
            background: #f1502f;
            cursor: pointer;
        }
        #explodeslider::-moz-range-thumb {
            width: 25px;
            height: 25px;
            background: #f1502f;
            cursor: pointer;
        }
      </style>
      <div id="info-box">
      <a id="filelink" href="#">File loading...</a></br>
      <input type="range" min="0" max="100" value="0" id="explodeslider" id="myRange">
      <\div>
      <div id="viewport">
      </div>`
  }

  constructor () {
    super()
    this.container = null
    this.animations = null
    this.explodeSlider = false
    this.camera = null
    this.scene = null
    this.renderer = null
    this.controls = null
  }

  firstUpdated () {
    this.init()
    this.renderScene()
  }

  addInCenter (object, up = false) {
    const box = new THREE.Box3().setFromObject(object)
    const size = box.getSize(new THREE.Vector3()).length()
    const center = box.getCenter(new THREE.Vector3())

    object.position.x += (object.position.x - center.x)
    object.position.y += (object.position.y - center.y)
    object.position.z += (object.position.z - center.z)
    this.camera.near = size / 100
    this.camera.far = size * 100
    this.camera.updateProjectionMatrix()
    this.camera.position.copy(center)
    this.camera.position.x += size / 2.0
    this.camera.position.y += size / 5.0
    this.camera.position.z += size / 2.0
    if (up) {
      this.camera.up = new THREE.Vector3(0, 0, 1)
    }
    this.camera.lookAt(center)
    this.scene.add(object)
    this.controls.update()
  }

  addLights (dirIntensity) {
    var amlight = new THREE.AmbientLight(0xFFFFFF, 0.3)
    this.camera.add(amlight)

    var dirlight1 = new THREE.DirectionalLight(0xFFFFFF, dirIntensity)
    dirlight1.position.set(0.5, 0, 0.866)
    this.camera.add(dirlight1)

    var dirlight2 = new THREE.DirectionalLight(0xFFFFFF, dirIntensity)
    dirlight2.position.set(0, 0.866, 0.5)
    this.camera.add(dirlight2)

    var dirlight3 = new THREE.DirectionalLight(0xFFFFFF, dirIntensity)
    dirlight3.position.set(0.866, 0.5, 0)
    this.camera.add(dirlight3)
  }

  init () {
    this.container = this.shadowRoot.querySelector('#viewport')
    this.camera = new THREE.PerspectiveCamera(60, this.offsetWidth / this.offsetHeight, 0.1, 1e10)

    this.scene = new THREE.Scene()
    this.scene.background = new THREE.Color(0xeeeeee)

    this.explodeSlider = this.shadowRoot.querySelector('#explodeslider')
    this.explodeSlider.addEventListener('input', this.explode.bind(this))

    // renderer

    this.renderer = new THREE.WebGLRenderer({ antialias: true })
    this.renderer.toneMapping = THREE.ACESFilmicToneMapping
    this.renderer.toneMappingExposure = 0.8
    this.renderer.outputEncoding = THREE.sRGBEncoding
    this.renderer.setPixelRatio(window.devicePixelRatio)
    this.renderer.setSize(this.offsetWidth, this.offsetHeight)
    this.container.append(this.renderer.domElement)

    var pmremGenerator = new THREE.PMREMGenerator(this.renderer)
    pmremGenerator.compileEquirectangularShader()

    new THREE.TextureLoader().load(machineshop, function (texture) {
      var envMap = pmremGenerator.fromEquirectangular(texture).texture

      this.scene.environment = envMap

      texture.dispose()
      pmremGenerator.dispose()

      var filetype = this.src.split('.').pop()
      var filelink = this.shadowRoot.querySelector('#filelink')
      filelink.href = this.src
      if (this['show-filelink'] !== 'false') {
        filelink.innerHTML = `Download ${this.src}`
      } else {
        filelink.innerHTML = ''
      }
      var loader
      if (['wrl', 'vrml', 'wrz'].includes(filetype.toLowerCase())) {
        loader = new VRMLLoader()
        loader.load(this.src, function (object) {
          this.addLights(0.1)
          this.addInCenter(object)
          this.renderScene()
        }.bind(this))
      } else if (filetype.toLowerCase() === 'stl') {
        var color
        if (this.color === undefined) {
          color = 0x0069f2
        } else {
          color = parseInt(standardizeColor(this.color).substr(1), 16)
        }
        loader = new STLLoader()
        loader.load(this.src, function (geometry) {
          var material = new THREE.MeshPhongMaterial({
            color: color,
            specular: 0xc6c6c6
          })
          var mesh = new THREE.Mesh(geometry, material)

          this.addLights(0.1)
          this.addInCenter(mesh, true)
          this.renderScene()
        }.bind(this))
      } else if (['gltf', 'glb'].includes(filetype.toLowerCase())) {
        this.renderScene()
        loader = new GLTFLoader()
        loader.load(this.src, function (gltf) {
          this.saveAnimations(gltf)
          this.addLights(0.1)
          this.addInCenter(gltf.scene)
          this.renderScene()
        }.bind(this))
      } else {
        filelink.innerHTML = `Download ${this.src} (Cannot view .${filetype} file)`
      }
    }.bind(this))
    this.scene.add(this.camera)

    // controls

    this.controls = new TrackballControls(this.camera, this.renderer.domElement)
    this.controls.addEventListener('change', this.renderScene.bind(this))

    this.controls.rotateSpeed = 4.0
    this.controls.zoomSpeed = 1.2
    this.controls.panSpeed = 0.8
    this.controls.keys = [65, 83, 68]
    this.controls.update()

    window.addEventListener('resize', this.onResize.bind(this), false)
  }

  saveAnimations (gltf) {
    this.animations = []
    gltf.animations.forEach(animation => {
      animation.tracks.forEach(track => {
        var animType = track.name.split('.').pop()
        if ((animType === 'position') && (track.values.length >= 6)) {
          var name = track.name.slice(0, -9)
          var start = track.values.slice(0, 3)
          var end = track.values.slice(-3)
          this.animations.push({ name: name, start: start, end: end })
        }
      })
    })
    if (this.animations.length > 0) {
      this.explodeSlider.style.visibility = 'visible'
    }
  }

  explode () {
    if ((this.animations != null) && (this.animations.length > 0)) {
      this.scene.children.forEach(child => {
        if (child.type === 'Group') {
          child.children.forEach(subchild => {
            if (subchild.type === 'Mesh') {
              this.animations.forEach(animation => {
                if (subchild.name === animation.name) {
                  subchild.position.x = animation.start[0] + this.explodeSlider.value / 100 * (animation.end[0] - animation.start[0])
                  subchild.position.y = animation.start[1] + this.explodeSlider.value / 100 * (animation.end[1] - animation.start[1])
                  subchild.position.z = animation.start[2] + this.explodeSlider.value / 100 * (animation.end[2] - animation.start[2])
                }
              })
            }
          })
        }
      })
      this.exploded = !this.exploded
      this.renderScene()
    }
  }

  onResize () {
    this.camera.aspect = this.offsetWidth / this.offsetHeight
    this.camera.updateProjectionMatrix()

    this.renderer.setSize(this.offsetWidth, this.offsetHeight)
    this.renderScene()
  }

  renderScene () {
    this.renderer.render(this.scene, this.camera)
  }
}

function standardizeColor (str) {
  var ctx = document.createElement('canvas').getContext('2d')
  ctx.fillStyle = str
  return ctx.fillStyle
}

window.customElements.define('gb3d-viewer', GB3DViewer)
